package com.prakash.spring.boot;

import https.www_sg_com.xml.employee.Employee;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class EmployeeRepository {
    private static final Map<String, Employee> employees = new HashMap<>();

    @PostConstruct
    public void initData() {

        Employee employee = new Employee();
        employee.setName("Jayaprakash");
        employee.setId(11291);
        employee.setLocation("Bangalore");
        employees.put(employee.getName(), employee);

        employee = new Employee();
        employee.setName("Raghav");
        employee.setId(11292);
        employee.setLocation("Bangalore");
        employees.put(employee.getName(), employee);

        employee = new Employee();
        employee.setName("Karthick");
        employee.setId(11293);
        employee.setLocation("Bangalore");
        employees.put(employee.getName(), employee);

        employee = new Employee();
        employee.setName("Sebi");
        employee.setId(11294);
        employee.setLocation("Bangalore");
        employees.put(employee.getName(), employee);
    }

    public Employee findEmployee(String name) {
        Assert.notNull(name, "The Employee's name must not be null");
        return employees.get(name);
    }
}
