package com.prakash.spring.boot;

import https.www_sg_com.xml.employee.EmployeeDetailsRequest;
import https.www_sg_com.xml.employee.EmployeeDetailsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class EmployeeEndpoint
{
    private static final String NAMESPACE_URI = "https://www.sg.com/xml/employee";

    private EmployeeRepository EmployeeRepository;

    @Autowired
    public EmployeeEndpoint(EmployeeRepository EmployeeRepository) {
        this.EmployeeRepository = EmployeeRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "EmployeeDetailsRequest")
    @ResponsePayload
    public EmployeeDetailsResponse getEmployee(@RequestPayload EmployeeDetailsRequest request) {
        EmployeeDetailsResponse response = new EmployeeDetailsResponse();
        response.setEmployee(EmployeeRepository.findEmployee(request.getName()));
        return response;
    }
}