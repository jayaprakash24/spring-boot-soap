package com.prakash.spring.boot;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

/* Config class extends WsConfigurerAdapter which configures annotation driven Spring-WS programming model */
@EnableWs
@Configuration
public class AppConfig extends WsConfigurerAdapter {

    /*
    MessageDispatcherServlet – Spring-WS uses it for handling SOAP requests.
    We need to inject ApplicationContext to this servlet so that Spring-WS find other beans.
    It also declares the URL mapping for the requests.
     */
    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(servlet, "/service/*");
    }

    /*
    DefaultWsdl11Definition exposes a standard WSDL 1.1 using XsdSchema.
    The bean name employeeDetailsWsdl will be the wsdl name that will be exposed.
    It will be available under http://localhost:8080/service/employeeDetailsWsdl.wsdl.
    This is the simplest approach to expose the contract first wsdl in spring.
     */
    @Bean(name = "employeeDetailsWsdl")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema countriesSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("EmployeeDetailsPort");
        wsdl11Definition.setLocationUri("/employee/employee-details");
        wsdl11Definition.setTargetNamespace("https://www.sg.com/xml/employee");
        wsdl11Definition.setSchema(countriesSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema employeeSchema() {
        return new SimpleXsdSchema(new ClassPathResource("employee.xsd"));
    }
}