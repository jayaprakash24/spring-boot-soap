package com.prakash.spring.boot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

@Component
public class EmployeeClient extends WebServiceGatewaySupport {

    private static final Logger log = LoggerFactory.getLogger(EmployeeClient.class);

    @Autowired
    WebServiceTemplate webServiceTemplate;

    /*
      getWebServiceTemplate() uses marshaller bean defined in AppConfig class
     */
    public Object getEmployee(String url, Object request) {
        return getWebServiceTemplate().marshalSendAndReceive(url, request);
    }

    public Object getEmployee(Object request) {
        return webServiceTemplate.marshalSendAndReceive(request);
    }
}
