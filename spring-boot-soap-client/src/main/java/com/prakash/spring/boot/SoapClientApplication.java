package com.prakash.spring.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoapClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(SoapClientApplication.class, args);
    }

   /* @Bean
    CommandLineRunner lookup(EmployeeClient employeeClient) {
        return args -> {
            String name = "Jayaprakash";
            if (args.length > 0) {
                name = args[0];
            }
            EmployeeDetailsRequest request = new EmployeeDetailsRequest();
            request.setName(name);
            EmployeeDetailsResponse response = (EmployeeDetailsResponse) employeeClient
                    .getEmployee("http://localhost:8080/service/employee-details", request);
            System.out.println("Got Response As below ========= : ");
            System.out.println("ID : " + response.getEmployee().getId());
            System.out.println("Name : " + response.getEmployee().getName());
            System.out.println("Location : " + response.getEmployee().getLocation());
        };
    }*/
}
