package com.prakash.spring.boot;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.transport.WebServiceMessageSender;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

@Configuration
public class EmployeeClientConfig {

    //@Value("${employee-client.default-uri}")
    private String defaultUri;

    @Value("${employee-client.connection-timeout}")
    private int connectionTimeout;

    @Value("${employee-client.read-timeout}")
    private int readTimeout;

     /*
     If we have multiple Webservice clients. we should not define marshaller bean
     */

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        // this package must match the package in the <generatePackage> specified in pom.xml
        marshaller.setContextPath("com.prakash.spring.boot.employee");
        return marshaller;
    }

   /* @Bean
    public EmployeeClient employeeClient(Jaxb2Marshaller marshaller) {
        EmployeeClient client = new EmployeeClient();
        client.setDefaultUri("http://localhost:8080/service/employee-details");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }*/


    @Bean
    public WebServiceTemplate webServiceTemplate() {
        WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
        webServiceTemplate.setMarshaller(marshaller());
        webServiceTemplate.setUnmarshaller(marshaller());
        webServiceTemplate.setDefaultUri("http://localhost:8082/service/employeeDetailsWsdl.wsdl");
        webServiceTemplate.setMessageSender(webServiceMessageSender());
        return webServiceTemplate;
    }

    @Bean
    public WebServiceMessageSender webServiceMessageSender() {
        HttpComponentsMessageSender httpComponentsMessageSender = new HttpComponentsMessageSender();
        httpComponentsMessageSender.setConnectionTimeout(connectionTimeout);
        httpComponentsMessageSender.setReadTimeout(readTimeout);
        return httpComponentsMessageSender;
    }
}
