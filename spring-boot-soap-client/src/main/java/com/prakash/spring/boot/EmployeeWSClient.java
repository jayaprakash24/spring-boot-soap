package com.prakash.spring.boot;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class EmployeeWSClient{

    public static void main(String args[]) throws MalformedURLException {

        URL url = new URL("http://localhost:8082/service/employeeDetailsWsdl.wsdl");
        QName qname = new QName("https://www.sg.com/xml/employee", "EmployeeDetailsPortService");
        Service service = Service.create(url, qname);
    }
}
