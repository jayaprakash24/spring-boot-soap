package com.prakash.spring.boot;

import https.www_sg_com.xml.employee_client.Employee;
import https.www_sg_com.xml.employee_client.EmployeeDetailsRequest;
import https.www_sg_com.xml.employee_client.EmployeeDetailsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class EmployeeClientEndpoint {
    private static final String NAMESPACE_URI = "https://www.sg.com/xml/employee-client";

    private EmployeeClient employeeClient;

    @Autowired
    public EmployeeClientEndpoint(EmployeeClient employeeClient) {
        this.employeeClient = employeeClient;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "EmployeeDetailsRequest1")
    @ResponsePayload
    public EmployeeDetailsResponse getEmployee(@RequestPayload EmployeeDetailsRequest request) {
        return prepareEmployeeDetailsResponse((com.prakash.spring.boot.employee.EmployeeDetailsResponse) employeeClient
                .getEmployee("http://localhost:8082/service/employee-details", prepareEmployeeDetailsRequest(request)));
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "EmployeeDetailsRequest")
    @ResponsePayload
    public EmployeeDetailsResponse getEmployeeWithURI(@RequestPayload EmployeeDetailsRequest request) {
        return prepareEmployeeDetailsResponse((com.prakash.spring.boot.employee.EmployeeDetailsResponse) employeeClient
                .getEmployee(prepareEmployeeDetailsRequest(request)));
    }

    private com.prakash.spring.boot.employee.EmployeeDetailsRequest prepareEmployeeDetailsRequest(
            EmployeeDetailsRequest employeeClientDetailsRequest) {
        com.prakash.spring.boot.employee.EmployeeDetailsRequest employeeDetailsRequest = new com.prakash.spring.boot.employee.EmployeeDetailsRequest();
        employeeDetailsRequest.setName(employeeClientDetailsRequest.getName());
        return employeeDetailsRequest;
    }

    private EmployeeDetailsResponse prepareEmployeeDetailsResponse(com.prakash.spring.boot.employee.EmployeeDetailsResponse employeeDetailsResponse) {
        EmployeeDetailsResponse employeeClientResponse = new EmployeeDetailsResponse();
        employeeClientResponse.setEmployee(prepareEmployee(employeeDetailsResponse.getEmployee()));
        return employeeClientResponse;
    }

    private Employee prepareEmployee(com.prakash.spring.boot.employee.Employee employee) {
        Employee employeeClient = new Employee();
        employeeClient.setId(employee.getId());
        employeeClient.setName(employee.getName());
        employeeClient.setLocation(employee.getLocation());
        return employeeClient;
    }
}
